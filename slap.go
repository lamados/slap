// Package slap implements Slap, an ordered, indexed, (mostly) thread-safe data structure with automatic deduplication.
package slap

import (
	"errors"
	"fmt"
	"sync"
)

// Slap is somewhere between a slice and a map.
type Slap[K comparable, V any] struct {
	slice   []V
	index   map[K]int
	keyFunc func(V) K
	len     int
	mu      sync.Mutex
}

// New returns a new Slap with the given key-value types and function for getting a key.
func New[K comparable, V any](keyFunc func(V) K) *Slap[K, V] {
	return &Slap[K, V]{
		index:   make(map[K]int),
		keyFunc: keyFunc,
	}
}

// Get returns a value with the given key from the Slap.
func (slap *Slap[K, V]) Get(key K) (value V, ok bool) {
	slap.mu.Lock()
	defer slap.mu.Unlock()

	i, ok := slap.index[key]
	if ok {
		value = slap.slice[i]
	}

	return value, ok
}

// Contains returns true if the Slap contains a given value.
func (slap *Slap[K, V]) Contains(value V) bool {
	slap.mu.Lock()
	defer slap.mu.Unlock()

	_, ok := slap.index[slap.keyFunc(value)]
	return ok
}

// Append appends values to the Slap.
func (slap *Slap[K, V]) Append(values ...V) {
	slap.mu.Lock()
	defer slap.mu.Unlock()

	for _, value := range values {
		key := slap.keyFunc(value)
		if _, ok := slap.index[key]; ok {
			continue
		}

		slap.slice = append(slap.slice, value)
		slap.index[key] = slap.len
		slap.len++
	}
}

// Remove removes elements with the given identifiers from the Slap.
func (slap *Slap[K, V]) Remove(keys ...K) {
	slap.mu.Lock()
	defer slap.mu.Unlock()

	for _, key := range keys {
		i, ok := slap.index[key]
		if !ok {
			continue
		}

		delete(slap.index, key)
		slap.slice = append(slap.slice[:i], slap.slice[i+1:]...)
		for _, value := range slap.slice[i:] {
			key := slap.keyFunc(value)
			i, _ := slap.index[key]
			slap.index[key] = i - 1
		}

		slap.len--
	}
}

// Length returns the current length of the Slap.
func (slap *Slap[K, V]) Length() int {
	return slap.len
}

// Scan iterates over each element in the Slap and runs a given function on them. If it returns false, iteration stops.
// It should be safe to call Get, Contains, or Append during iteration, however is not safe to call Remove and doing so
// may have unexpected results. Any values which are appended during iteration will have the given function run on them
// too, which may be seen as a feature or as unexpected behaviour depending on use-case.
func (slap *Slap[K, V]) Scan(iterFunc func(i int, value V) bool) {
	for i := 0; i < slap.Length(); i++ {
		if !iterFunc(i, slap.slice[i]) {
			return
		}
	}
}

// check checks the slice and index values to identify issues with
// TODO: figure out where/how this should be used.
func (slap *Slap[K, V]) check() error {
	slap.mu.Lock()
	defer slap.mu.Unlock()

	if len(slap.slice) != slap.len {
		return errors.New("length of slice is not equal to internal length counter")
		// fix: set internal length counter to length of slice.
	}

	for i := 0; i < slap.len; i++ {
		key := slap.keyFunc(slap.slice[i])
		if j, ok := slap.index[key]; ok {
			if i != j {
				return errors.New("value in slice returns key that does not correlate to self in index")
				// fix: reindex.
			}
		} else {
			return errors.New("slice contains value which is not available in index")
			// fix: add value to index.
		}
	}

	// no issues found.
	return nil
}

// reindex erases the Slap index and rebuilds it from the slice. If a key collision is detected, an error will be
// returned and the index will be in a malformed/incomplete state.
// TODO: figure out where/how this should be used.
func (slap *Slap[K, V]) reindex() error {
	slap.mu.Lock()
	defer slap.mu.Unlock()

	// clear index.
	for key := range slap.index {
		delete(slap.index, key)
	}

	// rebuild index from slice.
	for i, value := range slap.slice {
		key := slap.keyFunc(value)

		if _, ok := slap.index[key]; ok {
			return fmt.Errorf("%v resolves to a key which is already present in the index", value)
		}

		slap.index[key] = i
	}

	// reindex completed.
	return nil
}
